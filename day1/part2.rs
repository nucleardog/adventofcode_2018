use std::io;

fn main() {

	// Read input first

	let mut input: [i32; 1000] = [0; 1000];
	let mut input_size: usize = 0;

	loop {
		let mut line = String::new();
		let bytes_read = io::stdin().read_line(&mut line)
			.expect("Failed reading from stdin");

		if bytes_read == 0 {
			// Hit eof
			break;
		}

		// Remove the EOL character (well, the \n, sorry Windows)
		line.pop();

		assert!(input_size < input.len(), "Input is larger than input buffer");

		// Parse and put it in our input
		input[input_size] = line.parse().expect("Line was invalid format");
		input_size += 1;
	}


	// Process list to find duplicates

	let mut sum: i32 = 0;

	// Keep a list of all sums we've seen. We'll just used fixed size arrays
	// to keep life simple and failure-prone.
	let mut seen: [i32; 1000000] = [0; 1000000];
	// We initialize to 1 to seed our list with a single '0', since technically
	// our first sum (before we process the first input) is 0.
	let mut seen_count: usize = 1;

	let mut i: usize = 0;

	loop {
		// Update sum
		sum += input[i];

		// Check if it's a duplicate
		let mut found_duplicate: bool = false;
		for x in 0..seen_count {
			if seen[x] == sum {
				println!("Saw sum twice: {}", sum);
				found_duplicate = true;
				break;
			}
		}

		if found_duplicate == true {
		    break;
		}

		// Put it in the list if not
		assert!(seen_count < seen.len(), "run overflowed seen buffer size");

		seen[seen_count] = sum;
		seen_count += 1;
		i += 1;
		if i == input_size {
			i = 0;
		}
	}

}
