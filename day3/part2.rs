use std::io;

const SIZE: i32 = 1000;

fn main() {
	let mut grid: [i32; (SIZE * SIZE) as usize] = [0; (SIZE * SIZE) as usize];
	let mut claim_overlaps: [bool; 10000] = [false; 10000];
	let mut highest_claim: i32 = 0;

	loop {
		let mut line = String::new();
		let bytes_read = io::stdin().read_line(&mut line)
			.expect("Failed reading from stdin");

		if bytes_read == 0 {
			// Hit EOF
			break;
		}

		// Remove EOL character
		line.pop();

		let mut parms: [i32; 5] = [0; 5];
		let mut parm_idx: usize = 0;
		let mut buffer = String::new();

		for i in 0..line.chars().count() {
			let c: char = line.chars().nth(i).unwrap();

			if c.is_digit(10) {
				buffer.push(c);
			} else {
				assert!(parm_idx < 5, "Got too many parameters on line");
				if buffer.chars().count() != 0 {
					parms[parm_idx] = buffer.parse().expect("Got non-int buffer");
					buffer.clear();
					parm_idx += 1;
				}
			}
		}

		if buffer.chars().count() != 0 {
			assert!(parm_idx < 5, "Got too many parameters on line");
			parms[parm_idx] = buffer.parse().expect("Got non-int buffer");
			parm_idx += 1;
		}

		assert!(parm_idx == 5, "Line was invalid format");

		let claim_num = parms[0];
		let claim_x = parms[1];
		let claim_y = parms[2];
		let claim_w = parms[3];
		let claim_h = parms[4];

		if claim_num > highest_claim {
			highest_claim = claim_num;
		}

		// Check each cell we're claiming
		// If we're the first person to claim it, write our claim number to it.
		// If someone has already claimed it, mark ourselves as overlapping and
		// the other claim as overlapping.
		for y in claim_y..(claim_y+claim_h) {
			for x in claim_x..(claim_x+claim_w) {
				let idx: usize = ((y * SIZE) + x) as usize;
				if grid[idx] > 0 {
					claim_overlaps[claim_num as usize] = true;
					claim_overlaps[grid[idx] as usize] = true;
				} else {
					grid[idx] = claim_num;
				}
			}
		}

	}

	// Find results!
	for i in 1..highest_claim+1 {
		if claim_overlaps[i as usize] == false {
			println!("Claim {} does not overlap", i);
		}
	}

}

