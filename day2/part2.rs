use std::io;

fn main() {
	let mut input = Vec::new();
	let mut done: bool = false;

	loop {
		let mut line = String::new();
		let bytes_read = io::stdin().read_line(&mut line)
			.expect("Failed reading from stdin");

		if bytes_read == 0 {
			// hit eof
			break;
		}

		line.pop();
		input.push(line);
	}

	for i in 0..input.len() {
		for j in i+1..input.len() {
			if count_same(&input[i], &input[j]) == input[i].bytes().count() - 1 {
				// Found our matches
				print_same(&input[i], &input[j]);
				done = true;
				break;
			}
		}
		if done == true { break; }
	}

}

/**
 * Count the number of characters that are the same between two strings. The
 * same means the same byte, in the same position.
 *
 * Requires both strings are the same length.
 */
fn count_same(s1: &String, s2: &String) -> usize {
	let mut count: usize = 0;
	for i in 0..s1.bytes().count() {
		if s2.bytes().nth(i).unwrap() == s1.bytes().nth(i).unwrap() {
			count += 1;
		}
	}
	count
}

/**
 * Print any characters that are the same between two strings. The same means
 * the same byte in the same position.
 *
 * Requires both strings are the same length.
 */
fn print_same(s1: &String, s2: &String) {
	for i in 0..s1.bytes().count() {
		if s2.bytes().nth(i).unwrap() == s1.bytes().nth(i).unwrap() {
			print!("{}", s2.bytes().nth(i).unwrap() as char);
		}
	}
	println!();
}
