use std::io;

const SIZE: i32 = 1000;

fn main() {
	let mut grid: [i32; (SIZE * SIZE) as usize] = [0; (SIZE * SIZE) as usize];

	loop {
		let mut line = String::new();
		let bytes_read = io::stdin().read_line(&mut line)
			.expect("Failed reading from stdin");

		if bytes_read == 0 {
			// Hit EOF
			break;
		}

		// Remove EOL character
		line.pop();

		let mut parms: [i32; 5] = [0; 5];
		let mut parm_idx: usize = 0;
		let mut buffer = String::new();

		for i in 0..line.chars().count() {
			let c: char = line.chars().nth(i).unwrap();

			if c.is_digit(10) {
				buffer.push(c);
			} else {
				assert!(parm_idx < 5, "Got too many parameters on line");
				if buffer.chars().count() != 0 {
					parms[parm_idx] = buffer.parse().expect("Got non-int buffer");
					buffer.clear();
					parm_idx += 1;
				}
			}
		}

		if buffer.chars().count() != 0 {
			assert!(parm_idx < 5, "Got too many parameters on line");
			parms[parm_idx] = buffer.parse().expect("Got non-int buffer");
			parm_idx += 1;
		}

		assert!(parm_idx == 5, "Line was invalid format");

		let claim_x = parms[1];
		let claim_y = parms[2];
		let claim_w = parms[3];
		let claim_h = parms[4];

		// Count claims
		for y in claim_y..(claim_y+claim_h) {
			for x in claim_x..(claim_x+claim_w) {
				grid[((y * SIZE) + x) as usize] += 1;
			}
		}

	}

	// Find results!
	let mut count: i32 = 0;
	for y in 0..SIZE {
		for x in 0..SIZE {
			if grid[((y * SIZE) + x) as usize] >= 2 {
				count += 1;
			}
		}
	}

	println!("Found {} overlap", count);
}

