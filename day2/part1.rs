use std::io;

fn main() {

	let mut count_double: u32 = 0;
	let mut count_triple: u32 = 0;

	loop {
		let mut line = String::new();
		let bytes_read = io::stdin().read_line(&mut line)
			.expect("Failed reading from stdin");

		if bytes_read == 0 {
			// Hit eof
			break;
		}

		// Drop the EOL character
		line.pop();

		let mut is_double: bool = false;
		let mut is_triple: bool = false;

		while line.bytes().count() > 0 {
			let cur: u8 = line.bytes().next().unwrap();
			let mut count: u32 = 0;

			let mut i: usize = line.bytes().count() - 1;
			loop {
				if line.bytes().nth(i).unwrap() == cur {
					line.remove(i);
					count += 1;
				}
				if i == 0 {
					break;
				}
				i -= 1;
			}

			if count == 3 {
				is_triple = true;
			}
			else if count == 2 {
				is_double = true;
			}
		}

		if is_triple == true {
			count_triple += 1;
		}
		if is_double == true {
			count_double += 1;
		}
	}

	println!("Double: {}", count_double);
	println!("Triple: {}", count_triple);
	println!("Checksum: {}", (count_double * count_triple));
}
