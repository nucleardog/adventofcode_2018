use std::io;

fn main() {
    let mut sum: i32 = 0;

    loop {
        let mut line = String::new();
        let bytes_read = io::stdin().read_line(&mut line)
            .expect("Failed reading from stdin");
        line.pop();

        if bytes_read == 0 {
            break;
        }

        // Analyze line of input
        let as_int: i32 = line.parse().expect("Line was in invalid format");
        sum += as_int
    }

    // Print result
    println!("sum = {}", sum);
}
